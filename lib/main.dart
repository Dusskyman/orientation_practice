import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    MediaQueryData mediaQuery = MediaQuery.of(context);
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              mediaQuery.orientation == Orientation.portrait
                  ? 'Portrait'
                  : 'Landscape',
            ),
            Container(
              width: mediaQuery.size.width * 0.75,
              height: mediaQuery.size.height * 0.75,
              color: Colors.red,
            )
          ],
        ),
      ),
    );
  }
}
